package com.hmt.QCApp.Models.GeneralModels;

import org.json.JSONException;
import org.json.JSONObject;

public class User {


    public double  srl;
    public String  fname;
    public String  lname;
    public String  username;
    public String  psw;
    public String  macaddress;
    public String  qcareatSrl ;
    public boolean UserAuthentication ;
    public boolean MacIsValid ;
    public String UserAppVer;
    public String ServerReqVer;
    public boolean ClientVerIsValid;
    public boolean UserAuthorization;




    public String toJSON(User U){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("srl", U.srl);
            jsonObject.put("fname",U.fname );
            jsonObject.put("lname",U.lname );
            jsonObject.put("username",U.username);
            jsonObject.put("psw",U.psw);
            jsonObject.put("macaddress",U.macaddress);
            jsonObject.put("qcareatSrl",U.qcareatSrl);
            jsonObject.put("UserAuthentication",U.UserAuthentication);
            jsonObject.put("MacIsValid",U.MacIsValid);
            jsonObject.put("UserAppVer",U.UserAppVer);
            jsonObject.put("ServerReqVer",U.ServerReqVer);
            jsonObject.put("ClientVerValid",U.ClientVerIsValid);
            jsonObject.put("UserAuthorization",U.UserAuthorization);
            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

}
