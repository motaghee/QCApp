package com.hmt.QCApp.Models.QCModels;

import org.json.JSONException;
import org.json.JSONObject;

public class Car {
    public String vin="" ;
    public boolean validFormat = false;
    public boolean auditEditable  = false;
    public String msg="" ;
    public String joineryDate_Fa ="" ;
    public String aliasName="" ;
    public String grpName="" ;
    public String clrAlias="" ;
    public String ptTrace="" ;
    public String qcTrace="" ;


    public String toJSON(Car c){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("vin", c.vin);
            jsonObject.put("ValidFormat",c.validFormat );
            jsonObject.put("AuditEditable",c.auditEditable );
            jsonObject.put("AuditEditable",c.auditEditable );
            jsonObject.put("msg", c.msg);
            jsonObject.put("joineryDate_Fa", c.joineryDate_Fa);
            jsonObject.put("aliasName", c.aliasName);
            jsonObject.put("grpName", c.grpName);
            jsonObject.put("clrAlias", c.clrAlias);
            jsonObject.put("ptTrace", c.ptTrace);
            jsonObject.put("qcTrace", c.qcTrace);


            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }
}
