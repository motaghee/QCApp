package com.hmt.QCApp.Models.QCModels;

import org.json.JSONException;
import org.json.JSONObject;

public class Qccastt {

    public double Srl  = 0; //double
    public boolean ValidFormat  = false;
    public String Vin = "";
    public String VinWithoutChar = "";
    public String AreaDesc ;
    public String CreatedByDesc ;
    public String RepairedByDesc ;
    public String ModuleName ;
    public String DefectDesc ;
    public String CreatedDateFa ;
    public String StrenghtDesc ;
    public String Msg  = "";
    public double IsRepaired  = 0;


    public String toJSON(Qccastt c){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("Srl", c.Srl);
            jsonObject.put("Vin", c.Vin);
            jsonObject.put("ValidFormat",c.ValidFormat );
            jsonObject.put("VinWithoutChar",c.VinWithoutChar );
            jsonObject.put("AreaDesc",c.AreaDesc );
            jsonObject.put("CreatedByDesc", c.CreatedByDesc);
            jsonObject.put("RepairedByDesc", c.RepairedByDesc);
            jsonObject.put("ModuleName", c.ModuleName);
            jsonObject.put("DefectDesc", c.DefectDesc);
            jsonObject.put("CreatedDateFa", c.CreatedDateFa);
            jsonObject.put("StrenghtDesc", c.StrenghtDesc);
            jsonObject.put("IsRepaired", c.IsRepaired);
            jsonObject.put("Msg", c.Msg);



            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }
}
