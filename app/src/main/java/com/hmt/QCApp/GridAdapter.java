package com.hmt.QCApp;

//Import java libraries

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hmt.QCApp.Controllers.GeneralControllers.GetImageFromUrl;

import org.json.JSONArray;
import org.json.JSONObject;

public class GridAdapter extends BaseAdapter {
    private final Context Context;
    private final JSONArray productList;

    // Step 1
    public GridAdapter(Context context,JSONArray productList) {
        this.Context = context;
        this.productList = productList;
    }


    // Step 2
    @Override
    public int getCount() {
        return this.productList.length();
    }

    // Step 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Step 4
    @Override
    public JSONObject getItem(int position)  {
        JSONObject curJson = new JSONObject();
        try {
            curJson = this.productList.getJSONObject( position );
        } catch( Exception e ){
            e.printStackTrace();
        }
        return curJson;
    }

    // Step 5
    @Override
    public View getView(int position, View convertView, ViewGroup parent)  {


        LayoutInflater inflater = (LayoutInflater) Context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        // For single column
        View  gridView = inflater.inflate(R.layout.grid_adapter, null);
        // For double column
        // View  gridView = inflater.inflate(R.layout.grid_adapter_double_col, null);

        JSONObject SingleProduct = getItem(position);
        // Set adopter element into variable
        ImageView ProductImageView  = (ImageView) gridView.findViewById(R.id.product_image);
        TextView  ProductName       = (TextView) gridView.findViewById(R.id.product_name);
        TextView  ProductAmount     = (TextView) gridView.findViewById(R.id.product_amount);
        TextView  ProductSmlDesc    = (TextView) gridView.findViewById(R.id.product_sml_description);
        LinearLayout llRowMain =  (LinearLayout) gridView.findViewById(R.id.LLMain);
        if ((position==1)||(position==3))
            llRowMain.setBackgroundColor(Color.RED);

        //ProductName.setTextColor(Color.parseColor("#723688"));

        try {
            // Set into adabter imageview and textview
            new GetImageFromUrl(ProductImageView).execute(SingleProduct.getString("prod_thumb"));
            // ProductImageView.setImageDrawable(ContextCompat.getDrawable( Context, R.drawable.ic_camera_alt_black_24dp) );
            ProductName.setText( SingleProduct.getString("prod_name") );
            ProductAmount.setText( SingleProduct.getString("prod_price") );
            ProductSmlDesc.setText( SingleProduct.getString("prod_desc") );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gridView;
    }
}