package com.hmt.QCApp.Controllers.GeneralControllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.hmt.QCApp.Controllers.AuditControllers.AuditunlockcarActivity;
import com.hmt.QCApp.Models.GeneralModels.User;
import com.hmt.QCApp.Models.GeneralModels.clsAppInfo;
import com.hmt.QCApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    public String WifiMacAddress="";
    public String ClientAppVer="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        WifiMacAddress=getMacAddress();
        clsAppInfo.AuditAppVersion =ClientAppVer=getResources().getString(R.string.app_Version);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public void onClickbtnLogin(View v)
    {
        // --
        TextView txtUsername = findViewById(R.id.txtUsername);
        TextView txtPsw = findViewById(R.id.txtPsw);
        // --
        if ((!txtUsername.getText().toString().isEmpty())&&(!txtPsw.getText().toString().isEmpty()))
        // login user
        {
            //--- send to srv
            User SUser = new User();
            SUser.macaddress = WifiMacAddress;
            SUser.username = txtUsername.getText().toString();
            SUser.psw = txtPsw.getText().toString();
            Server exampleP = new Server();
            String json = SUser.toJSON(SUser);
            String response = null;
            try {
                response = exampleP.post("http://172.20.9.57:8080/api/users", json);
                //System.out.println(response);
                JSONObject Jobject = new JSONObject(response);
                User ruser = new User();
                ruser.UserAuthentication = Jobject.getBoolean("USERATHENTICATION");
                ruser.MacIsValid = Jobject.getBoolean("MACISVALID");
                ruser.ClientVerIsValid = Jobject.getBoolean("CLIENTVERISVALID");
                ruser.UserAuthorization = Jobject.getBoolean("USERAUTHORIZATION");
                if (ruser.ClientVerIsValid)
                {
                    if (ruser.UserAuthentication)
                    {
                        if (ruser.UserAuthorization)
                        {
                            if (ruser.MacIsValid) {
                                ruser.srl = Jobject.getDouble("SRL");
                                ruser.macaddress = Jobject.getString("MACADDRESS");
                                ruser.fname = Jobject.getString("FNAME");
                                ruser.lname = Jobject.getString("LNAME");
                                ruser.ServerReqVer = Jobject.getString("SERVERREQVER");
                                ruser.UserAppVer = ClientAppVer;
                                Toast.makeText(LoginActivity.this, " آقای " + ruser.lname + " به سیستم کیفیت محصول خوش آمدید ", Toast.LENGTH_SHORT).show();
                                // hide keyboard
                                InputMethodManager input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                input.hideSoftInputFromWindow(txtPsw.getWindowToken(), 0);
                                // goto to next activity
                                startActivity(new Intent(LoginActivity.this, AuditunlockcarActivity.class));
                                finish();
                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this, "دستگاه هوشمند شما در لیست دستگاه های مجاز تعریف نشده است", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "کدکاربری شما مجاز به استفاده از این سیستم نمی باشد", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                        Toast.makeText(LoginActivity.this, "اطلاعات ورود نادرست می باشد", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(LoginActivity.this, "لطفا ویرایش جدید نرم افزار را دریافت نمایید", Toast.LENGTH_SHORT).show();
                //txtUsername.setText(Jobject.toString()); // JO Good
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, "عدم برقراری ارتباط با شبکه ی سایپا", Toast.LENGTH_SHORT).show();
                //MyTxt.setText("Error on Run Post IOException 1");
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, "Err2", Toast.LENGTH_LONG).show();
                //txtUsername.setText(e.getMessage());
                //MyTxt.setText("Error on Run Post JSONException 2");
            }
            System.out.println(response);
        }
        else
            Toast.makeText(LoginActivity.this, "اطلاعات ورود را بطور کامل وارد نمایید", Toast.LENGTH_SHORT).show();


    }

    // get wifi mac address
    public String getMacAddress() {
        //--- get mac address--
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";

        //

    }


}
