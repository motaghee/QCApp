package com.hmt.QCApp.Controllers.QCControllers;

import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;
import android.widget.Toast;

import com.hmt.QCApp.Controllers.GeneralControllers.Server;
import com.hmt.QCApp.Models.QCModels.Qccastt;
import com.hmt.QCApp.R;

import org.json.JSONArray;

import java.io.IOException;

public class CardefectsActivity extends AppCompatActivity {

    private Context context;
    private GridView gridView;
    private JSONArray QcasttList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardefects);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        context = this;
        gridView = (GridView) findViewById(R.id.gvModuleDefects);
        renderQccasttList();
        if (!QcasttList.toString().isEmpty())
            createGridView();
        else
            Toast.makeText(this, "عدم دریافت اطلاعات چک لیست از سرور", Toast.LENGTH_SHORT).show();

    }
    private void createGridView(){
        // Create grid adapter
        MDGridAdapter MDGA = new MDGridAdapter( this, QcasttList );
        // Set grid adapter into GridView
        gridView.setAdapter( MDGA );
    }

    private void renderQccasttList()
    {
        // Sample data - you should get data from store sever same like format
        String sampleJsonString ="";
        String vin="NAS411100G1205277";
        //TextView txtVin= findViewById(R.id.txtVin);
        //txtVin.setText("NAS411100K1146021");
        if (true)//((!txtVin.getText().toString().isEmpty())&&(txtVin.getText().length()==17)) {
        {
            Qccastt c = new Qccastt();
            c.Vin = vin;
            c.IsRepaired=0;
            //--- ok http get sample
            //Server exampleP = new Server();
            Server exampleP = new Server();
            String json = c.toJSON(c);
            String response = null;
            try {
                response = exampleP.post("http://172.20.9.57:8080/api/qccastt",json);
                System.out.println(response);
                //JSONArray jsonarray = new JSONArray(strResponse);
                //JSONObject Jobject = new JSONObject(response);
                //Jobject.toString()
                sampleJsonString=response;
                try {

                    QcasttList = new JSONArray( sampleJsonString );
                } catch (Exception e){
                    Toast.makeText(this, "0Error on JSONArray"+e.getMessage(), Toast.LENGTH_LONG).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error on Run Post IOException"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
/*             catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error on Run Post JSONException"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
            */
            catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(this, "Error on Run Post Exception"+e.getMessage(), Toast.LENGTH_LONG).show();
            }

            System.out.println(response);

        }
        else
            Toast.makeText(this, "شماره شاسی را بطور صحیح وارد نمایید", Toast.LENGTH_SHORT).show();

    }
}
