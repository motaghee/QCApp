package com.hmt.QCApp.Controllers.QCControllers;

//Import java libraries

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hmt.QCApp.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class MDGridAdapter extends BaseAdapter {
    private final Context Context;
    private final JSONArray productList;

    // Step 1
    public MDGridAdapter(Context context, JSONArray productList) {
        this.Context = context;
        this.productList = productList;
    }


    // Step 2
    @Override
    public int getCount() {
        return this.productList.length();
    }

    // Step 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Step 4
    @Override
    public JSONObject getItem(int position)  {
        JSONObject curJson = new JSONObject();
        try {
            curJson = this.productList.getJSONObject( position );
        } catch( Exception e ){
            e.printStackTrace();
        }
        return curJson;
    }

    // Step 5
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)  {
        LayoutInflater inflater = (LayoutInflater) Context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        // For single column
        View  gridView = inflater.inflate(R.layout.mdgrid_adapter, null);
        // For double column
        // View  gridView = inflater.inflate(R.layout.grid_adapter_double_col, null);
        JSONObject SingleProduct = getItem(position);
        // Set adopter element into variable
        ImageView imgDefectStatus  = (ImageView) gridView.findViewById(R.id.imgDefectStatus);
        ImageView imgDefectType  = (ImageView) gridView.findViewById(R.id.imgDefectType);
        TextView  txtModuleName       = (TextView) gridView.findViewById(R.id.txtModuleName);
        TextView  txtDefectDesc     = (TextView) gridView.findViewById(R.id.txtDefectDesc);
        TextView  txtStrenghtDesc    = (TextView) gridView.findViewById(R.id.txtStrenghtDesc);
        TextView  txtCreatedDateFa    = (TextView) gridView.findViewById(R.id.txtCreatedDateFa);
        TextView  txtCreatedByDesc    = (TextView) gridView.findViewById(R.id.txtCreatedByDesc);
        TextView  txtRepairedByDesc    = (TextView) gridView.findViewById(R.id.txtRepairedByDesc);
        TextView  txtAreaDesc      = (TextView) gridView.findViewById(R.id.txtAreaDesc);
        // change in adapter when create
        //        LinearLayout llRowMain =  (LinearLayout) gridView.findViewById(R.id.LLMain);
        //        if ((position==1)||(position==3))
        //            llRowMain.setBackgroundColor(Color.RED);

        try {
            // Set into adabter imageview and textview
            // new GetImageFromUrl(ProductImageView).execute(SingleProduct.getString("prod_thumb"));
            // ProductImageView.setImageDrawable(ContextCompat.getDrawable( Context, R.drawable.ic_camera_alt_black_24dp) );
            txtModuleName.setText( SingleProduct.getString("ModuleName") );
            txtDefectDesc.setText( SingleProduct.getString("DefectDesc") );
            txtStrenghtDesc.setText( SingleProduct.getString("StrenghtDesc") );
            txtAreaDesc.setText( SingleProduct.getString("AreaDesc") );
            txtCreatedDateFa.setText( SingleProduct.getString("CreatedDateFa") );
            txtCreatedByDesc.setText( SingleProduct.getString("CreatedByDesc") );
            txtRepairedByDesc.setText( SingleProduct.getString("RepairedByDesc") );
            double IsRepaired=SingleProduct.getDouble("IsRepaired");
            String StrenghtDesc=SingleProduct.getString("StrenghtDesc");
            //txtInspector.setText( SingleProduct.getString("Inspector"));
            LinearLayout llRowMain =  (LinearLayout) gridView.findViewById(R.id.LLMain);
            //
            if (IsRepaired==1)
                imgDefectStatus.setImageDrawable(ContextCompat.getDrawable( Context, R.drawable.image4) );
            else
                imgDefectStatus.setImageDrawable(ContextCompat.getDrawable( Context, R.drawable.image3) );
            // --
            if (txtRepairedByDesc.toString().isEmpty())
                txtRepairedByDesc.setText("عدم دفع");
            // --
            if ( (StrenghtDesc.toString().equals("A")) ||(StrenghtDesc.toString().equals("S"))||(StrenghtDesc.toString().equals("P")))
                imgDefectType.setImageDrawable(ContextCompat.getDrawable( Context, R.drawable.warning1) );
                //llRowMain.setBackgroundColor(Color.parseColor("#84ffff"));
            // --
        } catch (Exception e) {
            e.printStackTrace();
        }
        Button btn = (Button) gridView.findViewById(R.id.btnRepair);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //string item_clicked = parent.getItemAtPosition(position).toString();
                Toast.makeText(Context.getApplicationContext(), "wo"+position+getItem(position).toString(), Toast.LENGTH_SHORT).show();
                //Toast here
                //Toast.makeText(Context.getApplicationContext(),"Item Clicked: " + ((TextView) v.findViewById(R.id.product_name)).getText(), Toast.LENGTH_SHORT).show();

            }});

        return gridView;
    }
}