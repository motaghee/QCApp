package com.hmt.QCApp.Controllers.AuditControllers;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.hmt.QCApp.Models.QCModels.Car;
import com.hmt.QCApp.Controllers.GeneralControllers.Server;
import com.hmt.QCApp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class AuditunlockcarActivity extends AppCompatActivity {


    private IntentIntegrator qrScan;
    Button buttonScan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auditunlockcar);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        qrScan = new IntentIntegrator(this);

        buttonScan = (Button) findViewById(R.id.btnScan);
        buttonScan.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  qrScan.initiateScan();
              }
         }
        );

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Go Edit Car In QCAuditSys", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });


        /*final TextView txtVin= findViewById(R.id.txtVin);
        txtVin.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange (View v,boolean hasFocus){

                if (txtVin.getText().length() == 0)
                {
                    txtVin.setText("NAS");
                }
            }
        });*/

    }

    public void onClickBtnUnlock(View v)
    {
        TextView txtVin= findViewById(R.id.txtVin);
        if ((!txtVin.getText().toString().isEmpty())&&(txtVin.getText().length()==17)) {
            Car c = new Car();
            c.vin = txtVin.getText().toString();
            //--- ok http get sample
            Server exampleP = new Server();
            String json = c.toJSON(c);

            String response = null;
            try {
                response = exampleP.post("http://172.20.9.57:8080/api/audit", json);
                System.out.println(response);
                JSONObject Jobject = new JSONObject(response);
                Car Cr = new Car();
                Cr.vin = Jobject.getString("VIN");
                Cr.auditEditable = Jobject.getBoolean("AUDITEDITABLE");
                Cr.validFormat = Jobject.getBoolean("VALIDFORMAT");
                Cr.msg = Jobject.getString("MSG");
                if (Cr.auditEditable)
                    Toast.makeText(AuditunlockcarActivity.this, "خودرو یک روز ازحالت قفل خارج گردید", Toast.LENGTH_SHORT).show();
                else
                {
                    Toast.makeText(AuditunlockcarActivity.this, "عملیاتی انجام نشد", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(AuditunlockcarActivity.this, "Error on Run Post IOException"+e.getMessage(), Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(AuditunlockcarActivity.this, "Error on Run Post JSONException"+e.getMessage(), Toast.LENGTH_LONG).show();
            }
            System.out.println(response);

        }
        else
            Toast.makeText(AuditunlockcarActivity.this, "شماره شاسی را بطور صحیح وارد نمایید", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast.makeText(AuditunlockcarActivity.this, "onActivityResult", Toast.LENGTH_SHORT).show();
        String vin="";
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
            {
                vin=result.getContents();
                TextView txtVin= findViewById(R.id.txtVin);
                txtVin.setText(vin);
                //Toast.makeText(AuditunlockcarActivity.this, vin, Toast.LENGTH_SHORT).show();

            }
        }
    }



}

