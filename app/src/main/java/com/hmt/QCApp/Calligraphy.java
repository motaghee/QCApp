package com.hmt.QCApp;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Calligraphy extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/BKOODB.TTF")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

}